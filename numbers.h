///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Midterm - Loops
///
/// @file    numbers.h
/// @version 1.0
///
/// Holds numbers to be summed up in a structure.
///
/// @brief Midterm - Loops - EE 205 - Spr 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

struct ThreeArraysStruct
{
    long array1[100];
    long array2[100];
    long array3[100];
};

extern struct ThreeArraysStruct threeArrays;

